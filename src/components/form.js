import React from 'react';

class Form extends React.Component{
    constructor(props){
        super(props);
        this.handlSubmit = this.handlSubmit.bind(this);
        this.state={
            title : ''
        }
        this.handleChange = this.handleChange.bind(this);
 
    }
    /**
     * change input and get value
     * change state
     * @param {event} e 
     */
    handleChange(e){
        let title = e.target.value;
        this.setState((prev)=>{
            return {
                title
            }
        });
    }
    /**
     * get updated props after changing values in parent
     * Componetn (App) and rerender form
     * @param {Object} props 
     * @param {Object} state 
     */
    componentWillReceiveProps(props,state){
        // console.log(props);
        if(props.editTodo)
        this.setState((prev)=>{
            return {
                title: props.editTodo.todo
            }
        });
    }

    /**
     * save or update todo 
     * by cheking props
     * @param {event} e 
     */
    handlSubmit(e){
        e.preventDefault();
        let todo  = e.target.querySelector('input[name=title]').value.trim();
        let index = (e.target.querySelector('input[name=index]').value.trim());
        // console.log(index,todo); 

        if(todo){
            if(index)
                this.props.onUpdateTodo(index,todo);
            else    
                this.props.onAddTodo(todo);     
        }else{
            alert("fill todo please");
        }

    }

    render(){
        return (
            <div className="col-md-12 mt-5">
             <form onSubmit={(e)=>this.handlSubmit(e)}> 
                <input type="hidden" name="index" value={this.props.editTodo ? this.props.editTodo.index : ''} />
              <div className="form-group">
                  <label>Title</label>
                  <input type="text" name="title" className="form-control"
                            onChange={(e)=>this.handleChange(e)} 
                            value={ this.state.title } />
              </div>
            
              <div className="col-md-12">
                <button type="submit" className="btn btn-success float-right">
                    { this.props.editTodo && this.props.editTodo.todo  ? 'Update' : 'Add'}
                </button>
              </div>  
              </form>   
            </div>
        );
    }
}

export default Form;