import React from 'react';

class Index extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="col-md-12 mt-2">
                <ul style={ {listStyle:'none',paddingLeft:"0"} }>
                { 
                    this.props.todos.length ? 
                        this.props.todos.map((item,i)=><li 
                                                        key={ 'tod_list_'+i } 
                                                        style={ {display:"inline-block",width:"100%",margin:"5px 0"} 
                                                    }> {item} 

                                                    
                                                    
                                                    
                                                                <a href="" 
                                                                className="btn btn-sm btn-danger float-right "
                                                                onClick={(e)=>{e.preventDefault();this.props.onRemoveTodo(i);}}
                                                                >Remove</a> 
                                                                
                                                                <a href="javascript:void(0)" 
                                                                className="btn btn-sm btn-info float-right mr-2"
                                                                onClick={ (e)=>{e.preventDefault();this.props.onEditTodo(i)}}
                                                                >Edit</a>   </li>)
                    :
                    <li key={ 'not_found_todo' } style={ {display:"inline-block",width:"100%",margin:"5px 0"} }>Todos empty  </li>
                }
                
                </ul>
            </div>
        );
    }

}

export default Index;