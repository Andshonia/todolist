import React, { Component } from 'react';
import Form from  './components/form';
import List from './components/index';
class App extends Component {
  constructor(){
    super();
    
    this.state = {
        editTodo : null, // for edit todo
        todos    : ["first ","secend"]
    };
    this.addTodo    = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    this.editTodo   = this.editTodo.bind(this);
    this.updateTodo   = this.updateTodo.bind(this);
  }
  /**
   * find item by index and change value
   * update state for rerendering
   * @param {Integer} index 
   * @param {String} todo 
   */
  updateTodo(index,todo){
    let {todos} = this.state;
    todos[index]=todo;

    this.setState((prev)=>{
      return {
        editTodo : {index:'',todo:''},
        todos
      }
    });

  }
  /**
   * add to state an element
   * @param {String} todo 
   */
  addTodo(todo){
    let {todos} = this.state;

    if(todos.filter((item)=>item==todo).length){
      alert("this todo exists");
    }else{
      todos.push(todo);
      this.setState(todos);
    }
    
  }
  /**
   * filter state end except an alement by index
   * and set values after filter in state
   * @param {Integer} index 
   */
  removeTodo(index){
    let todos = this.state.todos.filter((item,i)=>i!=index);
    this.setState((prev)=>{
      return {
        todos
      }
    });
    
  }
  /**
   * find an alement in state 
   * and change index of Object editTodo
   * for rerendering add form 
   * @param {Integer} index 
   */
  editTodo(index){
    let todo = this.state.todos[index];
    let editTodo = {index,todo};
    this.setState((prev)=>{
      return {editTodo};
    });
  }
 
  render() {
    return ( 
      <div className="container">
          <div className="row">
              

            <Form 
                onAddTodo={this.addTodo} 
                editTodo={this.state.editTodo} 
                onUpdateTodo = {this.updateTodo}
            />

           <List 
                todos={this.state.todos} 
                onRemoveTodo={this.removeTodo}
                onEditTodo = {this.editTodo}
             />

             
          </div>
      </div>
     
    );
  }
}

export default App;
